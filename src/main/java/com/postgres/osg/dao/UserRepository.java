package com.postgres.osg.dao;

import com.postgres.osg.entity.User;

import java.util.List;

public interface UserRepository {
    List<User> findAll();

    User findOne(Integer id);

    void insertUser(User user);

    void updateUser(User user);

    void executeUpdateUser(User user);

    void deleteUser(User user);
}