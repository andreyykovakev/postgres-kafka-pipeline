package com.postgres.osg.dao;

import com.postgres.osg.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrmUserRepository extends CrudRepository<User, Integer> {
}
