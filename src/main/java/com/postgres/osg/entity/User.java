package com.postgres.osg.entity;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String position;
    private String name;
    private String imageUrl;

    public User() {
    }

    public User(String position,
                String name,
                String imageUrl) {
        this.position = position;
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public User(Integer id,
                String position,
                String name,
                String imageUrl) {
        this.id = id;
        this.position = position;
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public Integer getId() {
        return id;
    }

    public String getPosition() {
        return position;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
