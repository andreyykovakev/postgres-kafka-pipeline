package com.postgres.osg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OsgApplication {
	public static void main(String[] args) {
		SpringApplication.run(OsgApplication.class, args);
	}

}
