package com.postgres.osg.controllers;


import com.postgres.osg.dao.OrmUserRepository;
import com.postgres.osg.entity.User;
import com.postgres.osg.kafka.KafkaUserMessagingService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author andreikovalev on 5/13/19
 * @project lambda-api-spring
 */

@RestController
@RequestMapping(path = "/users",
        produces = "application/json")
@CrossOrigin(origins = "*")
public class UserController {

    private OrmUserRepository userRepository;
    private KafkaUserMessagingService kafkaUserMessagingService;

    @Autowired
    public UserController(OrmUserRepository userRepository,
                          KafkaUserMessagingService kafkaUserMessagingService) {
        this.userRepository = userRepository;
        this.kafkaUserMessagingService = kafkaUserMessagingService;
    }

    @GetMapping(path = "")
    public List<User> getAll() {

        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        return users;
    }

    @GetMapping(path = "/{id}")
    public User getTestimonial(@PathVariable Integer id) {
        Optional<User> userOptional = userRepository.findById(id);
        return userOptional.orElse(null);
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public void postTestimonial(@RequestBody User user) {
        kafkaUserMessagingService.saveUser(user);
    }

}
