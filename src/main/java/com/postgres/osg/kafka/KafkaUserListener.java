package com.postgres.osg.kafka;

import com.postgres.osg.dao.OrmUserRepository;
import com.postgres.osg.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaUserListener {

    private OrmUserRepository userRepository;

    @Autowired
    public KafkaUserListener(OrmUserRepository userRepository) {
        this.userRepository = userRepository;
    }
    @KafkaListener(topics="customTopic")
    public void handle(User user) {
        userRepository.save(user);
        System.out.println("saved " + user);
    }
}