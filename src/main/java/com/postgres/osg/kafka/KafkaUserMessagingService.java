package com.postgres.osg.kafka;

import com.postgres.osg.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaUserMessagingService {

    private KafkaTemplate<String, User> kafkaTemplate;

    //This bean is configured with Spring auto-configuration, autowiring works properly
    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public KafkaUserMessagingService(
            KafkaTemplate<String, User> kafkaTemplate) {
            this.kafkaTemplate = kafkaTemplate;
    }

    public void saveUser(User user) {
        kafkaTemplate.send("customTopic", user);
    }
}